﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TDoor_interact : MonoBehaviour {

    public float gazeTime = 5f;

    private float timer;

    private bool gazeAt;

    public Animation DOanim;

    private int Dpos_Index;


	void Start () {

       DOanim = GetComponent<Animation>();
	}
	
	void Update () {
        if (gazeAt)
        {
            timer += Time.deltaTime;
            if (timer >= gazeTime)
            {
                ExecuteEvents.Execute(gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.pointerDownHandler);
                timer = 0f;
                GetComponent<Collider>().enabled = false;
            }
        }
		
	}

    // is the door open or closed
    public void PlayDoorAnim()
    {
        if (!DOanim.isPlaying)
        {
            if (Dpos_Index == 0)
            {
                DOanim.Play("Door");
                Dpos_Index = 1;
            }
            else
            {
                DOanim.Play("DoorClosing");
                Dpos_Index = 0;
            }
        }
    }

    public void PointerEnter()
    {
        gazeAt = true;
    }
    public void PointerExit()
    {
        gazeAt = false;
        GetComponent<Collider>().enabled = true;     
    }
   
    // i wanna still change color of circle after trigger event
    public void PointerDown()
    {
        PlayDoorAnim();
    }
    
}

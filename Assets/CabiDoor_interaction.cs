﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CabiDoor_interaction : MonoBehaviour {

    public float gazeTime = 5f;

    private float timer;

    private bool gazeAt;

    public Animation CabiD_anim;

    private int Dpos_Index;

	
	void Start () {
        CabiD_anim = GetComponent<Animation>();
	}
	
	void Update () {
        if (gazeAt)
        {
            timer += Time.deltaTime;
            if (timer >= gazeTime)
            {
                ExecuteEvents.Execute(gameObject, new PointerEventData(EventSystem.current), ExecuteEvents.pointerDownHandler);
                timer = 0f;
                GetComponent<Collider>().enabled = false;
            }
        }
	}

    public void PlayDoorAnim()
    {
        if (!CabiD_anim.isPlaying)
        {
            if (Dpos_Index == 0)
            {
                CabiD_anim.Play("CabiDoor");
                Dpos_Index = 1;
            }
            else
            {
                CabiD_anim.Play("CabiDoorC");
                Dpos_Index = 0;
            }
        }
    }

    public void PointerEnter()
    {
        gazeAt = true;
    }
    public void PointerExit()
    {
        gazeAt = false;
        GetComponent<Collider>().enabled = true;
    }

    public void PointerDown()
    {
        PlayDoorAnim();
    }
}

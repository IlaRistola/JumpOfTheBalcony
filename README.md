"Jump of the balcony" is little Unity scene for testing google vr.
Test video: https://youtu.be/UJe5e7erBK0

There is some ideas to turn this as little vr-game, it's easy to bring some more
rooms if we talk about hotel rooms and corridors. But before this, we should make
good story inside it and thing more goals.

I wanna use full hands free controls for walking and touching objects. So I can 
just give vr-glasses for anyone and he can start testing immediately.

Walking and touch inputs work very well. I was planning to use events controller
for animations, but it seems to stop working on time to time. So I decide to do
it with pure script.

I think there is still much to do with visual look. Some textures are just
placeholders and lights aren't yet made ready. Audio also still on the way.

After all fps look's pretty good and stable with this un-ready wholeness.

Script folder is under Assets folder ;)

Creator: Ila Ristola 2017
